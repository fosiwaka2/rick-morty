export interface Ruta {
    key: number;
    path: string;
    element: JSX.Element;
}